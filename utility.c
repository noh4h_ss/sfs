#include "utility.h"

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>

void full_pread(int fd, void *buf, size_t count, off_t offset)
{
  ssize_t ret = pread(fd, buf, count, offset);
  if(ret == -1) {
    fprintf(stderr, "read error (%lu,%lu):", offset, count);
    perror("");
    exit(EXIT_FAILURE);
  } 

  if(ret < count) {
    fprintf(stderr, "nonfull read: offset = %lu count = %lu returned %ld\n", offset, count, ret);
    exit(EXIT_FAILURE);
  }
}


void full_pwrite(int fd, const void *buf, size_t count, off_t offset)
{
  ssize_t ret = pwrite(fd, buf, count, offset);
  if(ret == -1) {
    fprintf(stderr, "write error (%lu,%lu):", offset, count);
    perror("");
    exit(EXIT_FAILURE);
  } 

  if(ret < count) {
    fprintf(stderr, "nonfull write: offset = %lu count = %lu returned %ld\n", offset, count, ret);
    exit(EXIT_FAILURE);
  }
}


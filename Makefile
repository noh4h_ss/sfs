CFLAGS = -Wall

all: mkfs.sfs mount.sfs

clean:
	rm mkfs.sfs mount.sfs *.o

mkfs.sfs: sfs.h mkfs.sfs.c utility.o
	gcc $(CFLAGS) -o mkfs.sfs mkfs.sfs.c utility.o
	
mount.sfs: sfs.h mount.sfs.c utility.o
	gcc $(CFLAGS) `pkg-config fuse3 --cflags --libs` -o mount.sfs mount.sfs.c utility.o

utility.o: utility.c utility.h
	gcc $(CFLAGS) -c utility.c

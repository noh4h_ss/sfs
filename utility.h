#pragma once

#include <time.h>
#include <unistd.h>

#ifndef max
  #define max(a,b) (((a) > (b)) ? (a) : (b))
#endif

#ifndef min
  #define min(a,b) (((a) < (b)) ? (a) : (b))
#endif

#define SETTIME_NOW(t) (t = time(NULL));

void full_pread(int fd, void *buf, size_t count, off_t offset);

void full_pwrite(int fd, const void *buf, size_t count, off_t offset);



#define _GNU_SOURCE

#include <assert.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <fcntl.h>
#include <linux/fs.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include "sfs.h"
#include "utility.h"

static struct sfs_dentry cur_dir = {
  .ino = 1,
  .type = DENTRY_TYPE_DIR,
  .name_len = 1
};

static struct sfs_dentry parent_dir = {
  .ino = 1,
  .type = DENTRY_TYPE_DIR,
  .name_len = 2
};

int main(int argc, char* argv[])
{
  char* volume;
  int fd;
  struct sfs_superblock sb;
  struct sfs_inode inode;
  struct stat stbuf;
  uint64_t dev_size;
  uint64_t req_size;
  uint32_t last_bgroup_nblocks;
  int cur_dentry_size;
  int parent_dentry_size;
  uint8_t* buf;
  
  
  if(argc < 2) {
    fprintf(stderr, "Usage:\t mkfs.sfs  <device|image_file> [block size]\n");
    exit(EXIT_FAILURE);
  }
  
  volume = argv[1];
  
  fd = open(volume, O_RDWR | O_SYNC);
  if(fd == -1) {
    fprintf(stderr, "open: %s: %s\n", volume, strerror(errno));
    exit(EXIT_FAILURE);
  }
  
  if(fstat(fd, &stbuf) == -1) {
    perror("fstat()");
    exit(EXIT_FAILURE);
  }
  
  if(S_ISREG(stbuf.st_mode)) {
    dev_size = stbuf.st_size;
  } else if(S_ISBLK(stbuf.st_mode)) {
    if(ioctl(fd, BLKGETSIZE64, &dev_size) == -1) {
      perror("ioctl-BLKGETSIZE64");
      exit(EXIT_FAILURE);
    }
  } else {
    fprintf(stderr, "volume must be either a block device or a regular file\n");
    exit(EXIT_FAILURE);
  }
  
  if(dev_size <= (1<<24)) {
    fprintf(stderr, "volume size must be at least 16MB\n");
    exit(EXIT_FAILURE); 
  }

  sb.magic = MAGIC_VALUE;
  
  if(argc > 2) {
    sb.block_size = atoi(argv[2]);
    if(sb.block_size < 512 || sb.block_size > (1<<14) || (sb.block_size & (sb.block_size-1))) {
      fprintf(stderr, "%s is invalid block size\n", argv[2]);
      exit(EXIT_FAILURE);
    }
  } else {
    sb.block_size = 4096;
  }
  
  
  sb.bgroup_pref_size = 2 * sb.block_size + 8 * sb.block_size * INODE_DISK_SIZE;
  sb.bgroup_size = sb.bgroup_pref_size + 8 * sb.block_size * sb.block_size;

  for(sb.max_bgroup = 1; ; ++sb.max_bgroup) {
    sb.bgroups_start = (sizeof(struct sfs_superblock) + sb.max_bgroup * sizeof(struct sfs_bgroup_info) + (PG_SIZE-1)) & ~(PG_SIZE-1);
    req_size = sb.bgroups_start + (uint64_t)(sb.max_bgroup-1) * sb.bgroup_size + sb.bgroup_pref_size;

    if(req_size > dev_size) {
      --sb.max_bgroup;
      break;
    }
  }
  
  sb.bgroups_start = (sizeof(struct sfs_superblock) + sb.max_bgroup * sizeof(struct sfs_bgroup_info) + (PG_SIZE-1)) & ~(PG_SIZE-1);
  req_size = sb.bgroups_start + (uint64_t)(sb.max_bgroup-1) * sb.bgroup_size + sb.bgroup_pref_size;
  last_bgroup_nblocks = min(8*sb.block_size, (dev_size - req_size) / sb.block_size);
  assert(req_size <= dev_size);

  
  sb.total_inodes = sb.max_bgroup * 8 * sb.block_size;
  sb.total_blocks = (sb.max_bgroup-1) * 8 * sb.block_size + last_bgroup_nblocks;
  sb.free_inodes = sb.total_inodes - 1;
  sb.free_blocks = sb.total_blocks - 1;
  full_pwrite(fd, &sb, sizeof(sb), 0);
  
  
  buf = calloc(1, sb.block_size);
  
  for(uint32_t i = 0; i < sb.max_bgroup; ++i) {
    struct sfs_bgroup_info bgi;
    bgi.free_inodes = (i ? (8 * sb.block_size) : (8 * sb.block_size - 1));
    bgi.free_blocks = (i != sb.max_bgroup-1 ? (8 * sb.block_size) : last_bgroup_nblocks);  
    if(i == 0)
      bgi.free_blocks = 8*sb.block_size-1;
    
    full_pwrite(fd, &bgi, sizeof(bgi), sizeof(sb) + i * sizeof(bgi));
  
    if(i > 0) {
      full_pwrite(fd, buf, sb.block_size, sb.bgroups_start + (uint64_t)i*sb.bgroup_size);
      if(i < sb.max_bgroup-1) {
        full_pwrite(fd, buf, sb.block_size, sb.bgroups_start + (uint64_t)i*sb.bgroup_size + sb.block_size);  
      }  
    }
  }
  
    
  buf[0] = 1;
  full_pwrite(fd, buf, sb.block_size, sb.bgroups_start);
  full_pwrite(fd, buf, sb.block_size, sb.bgroups_start+sb.block_size);
  buf[0] = 0;
  
  
  
  memset(buf, 0, sb.block_size);
  for(int i = last_bgroup_nblocks; i < 8*sb.block_size; ++i)
    buf[i/8] |= (1<<(i&7));
  full_pwrite(fd, buf, sb.block_size, sb.bgroups_start + (uint64_t)(sb.max_bgroup-1)*sb.bgroup_size + sb.block_size);  

  
  cur_dentry_size = DENTRY_SIZE(&cur_dir);
  parent_dentry_size = DENTRY_SIZE(&parent_dir);
  memcpy(buf, &cur_dir, sizeof(struct sfs_dentry));
  buf[sizeof(struct sfs_dentry)] = '.';
  memcpy(buf+cur_dentry_size, &parent_dir, sizeof(struct sfs_dentry));
  memcpy(buf+cur_dentry_size+sizeof(struct sfs_dentry), "..", 2);
  full_pwrite(fd, buf, sb.block_size, sb.bgroups_start + sb.bgroup_pref_size);  
  
  
  memset(&inode, 0, INODE_DISK_SIZE);
  inode.mode = S_IFDIR | S_IRWXU | S_IRWXG | S_IRWXO;
  inode.uid = getuid();
  inode.gid = getgid();
  inode.atime = time(NULL);
  inode.mtime = time(NULL);
  inode.ctime = time(NULL);
  inode.nlink = 2;
  inode.bptr[0] = (sb.bgroups_start + sb.bgroup_pref_size) / sb.block_size;
  inode.size = cur_dentry_size + parent_dentry_size;
  inode.blocks = sb.block_size / 512;
  
  full_pwrite(fd, &inode, INODE_DISK_SIZE, sb.bgroups_start + 2 * sb.block_size);
    
  return 0;
}

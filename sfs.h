#pragma once

#include <stddef.h>
#include <stdint.h>
#include <sys/stat.h>
#include <sys/types.h>

#include <pthread.h>

#define PG_SIZE 4096

#define MAGIC_VALUE 0xABCD

#define N_BPTR0 5

struct sfs_superblock {
  uint16_t magic;
  uint16_t block_size;
  uint32_t max_bgroup;
  uint32_t total_inodes;
  uint32_t total_blocks;
  uint32_t free_inodes;
  uint32_t free_blocks;
  
  uint32_t bgroup_size;
  uint32_t bgroup_pref_size;
  uint32_t bgroups_start;
};

struct sfs_bgroup_info {
  uint32_t free_inodes;
  uint32_t free_blocks;
};

struct sfs_inode {
  struct {
    uint64_t size;
    uint32_t blocks;
    uint32_t atime;
    uint32_t mtime;
    uint32_t ctime;
    uint32_t bptr[N_BPTR0+3];
    mode_t mode;
    nlink_t nlink;
    uid_t uid;
    gid_t gid;
  };
  
  uint32_t ino;
  struct sfs_inode* next;
  pthread_rwlock_t lock;
  pthread_rwlock_t bptr_lock;
  
  uint64_t version;
};

#define INODE_DISK_SIZE offsetof(struct sfs_inode, ino)

struct sfs_dentry {
  uint32_t ino;
  uint8_t type;
  uint8_t name_len;
  char name[0];
} __attribute__((packed));

#define DENTRY_SIZE(dentry) (((dentry)->name_len + sizeof(struct sfs_dentry) + 3) & ~3u)

#define EMPTRY_DIR_SIZE 16

#define DENTRY_TYPE_REGFILE (S_IFREG>>12)
#define DENTRY_TYPE_DIR (S_IFDIR>>12)
#define DENTRY_TYPE_SLINK (S_IFLNK>>12)

#define DENTRY_TYPE(mode) ((mode & S_IFMT) >> 12)



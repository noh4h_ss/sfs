#include <assert.h>
#include <errno.h>
#include <limits.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <fcntl.h>
#include <linux/loop.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <sys/statfs.h>
#include <sys/types.h>
#include <unistd.h>

#include "utility.h"
#include "sfs.h"

#include <pthread.h>

#define FUSE_USE_VERSION 30
#include <fuse_lowlevel.h>
#include <fuse_opt.h>

#define dmsg(format, ...) fprintf(stderr, format "\n", __VA_ARGS__);

#define MAX_NAME_LEN 255
#define NO_TIMEOUT 1000000000.0

char zero_page[PG_SIZE];


int gfd;

struct sfs_superblock sb;

uint32_t block_size;
uint64_t block_mask;
uint64_t bptr0_lim;
uint64_t bptr1_cap;
uint64_t bptr1_lim;
uint64_t bptr2_cap;
uint64_t bptr2_lim;
uint64_t bptr3_cap;
uint64_t bptr3_lim;

struct sfs_bgroup_info* bgi;

pthread_mutex_t blocks_io_lock = PTHREAD_MUTEX_INITIALIZER; 
pthread_mutex_t inodes_io_lock = PTHREAD_MUTEX_INITIALIZER;

uint8_t* inode_bitmap;
uint8_t* f_inode_bitmap;
uint8_t* block_bitmap;
uint8_t* f_block_bitmap;

void init_blocks_io()
{
  block_size = sb.block_size;
  block_mask = block_size-1;
  
  bptr0_lim = N_BPTR0 * block_size;
  bptr1_cap = (block_size / 4) * block_size;
  bptr1_lim = bptr0_lim + bptr1_cap;
  bptr2_cap = (block_size / 4) * bptr1_cap;
  bptr2_lim = bptr1_lim + bptr2_cap;
  bptr3_cap = (block_size / 4) * bptr2_cap;
  bptr3_lim = bptr2_lim + bptr3_cap;

  bgi = malloc(sb.max_bgroup * sizeof(struct sfs_bgroup_info));
  full_pread(gfd, bgi, sb.max_bgroup * sizeof(struct sfs_bgroup_info), sizeof(struct sfs_superblock));

  inode_bitmap = malloc(block_size);
  f_inode_bitmap = malloc(block_size);
  block_bitmap = malloc(block_size);
  f_block_bitmap = malloc(block_size);
  
  assert(inode_bitmap && f_inode_bitmap && block_bitmap && f_block_bitmap);
}

uint32_t new_block()
{
  static int idx = 0;
  static int bm_idx = 0;
  int bit_idx;
  
  uint64_t bm_off;
  uint32_t ret;
  
  pthread_mutex_lock(&blocks_io_lock);
  
  if(sb.free_blocks == 0) {
    pthread_mutex_unlock(&blocks_io_lock);
    return 0;
  }
  
  while(bgi[idx].free_blocks == 0) {
    ++idx;
    if(idx == sb.max_bgroup)
      idx = 0;
      
    bm_idx = 0;
  }
  

  bm_off = sb.bgroups_start + (uint64_t)idx * sb.bgroup_size + sb.block_size;
  
  full_pread(gfd, block_bitmap, sb.block_size, bm_off);
  
  while(block_bitmap[bm_idx] == 0xFF) {
    ++bm_idx;
    if(bm_idx == block_size)
      bm_idx = 0;
  }
      
  bit_idx = 0;
  while(block_bitmap[bm_idx] & (1 << bit_idx))
    ++bit_idx;
   
    
  block_bitmap[bm_idx] |= (1 << bit_idx);
  
  full_pwrite(gfd, block_bitmap, sb.block_size, bm_off);
  
  ret = (bm_off - block_size + sb.bgroup_pref_size) / block_size + (8*bm_idx+bit_idx);
  
  --bgi[idx].free_blocks;
  --sb.free_blocks;
  
    if(idx == sb.max_bgroup-1) {
      dmsg("last bgroup %d", bgi[idx].free_blocks);
    }
  
  
  pthread_mutex_unlock(&blocks_io_lock);
  
  return ret;
}

void free_block(uint32_t block)
{
  uint64_t off;
  uint32_t idx;
  uint64_t bm_off;
  uint32_t bm_idx; 
  
  off = (uint64_t)block * sb.block_size;
  idx = (off - sb.bgroups_start) / sb.bgroup_size;
  bm_off = sb.bgroups_start + (uint64_t)idx * sb.bgroup_size + sb.block_size;
  bm_idx = (off - (bm_off - block_size + sb.bgroup_pref_size)) / block_size;

  pthread_mutex_lock(&blocks_io_lock);

  full_pread(gfd, f_block_bitmap, sb.block_size, bm_off);
  
  assert(f_block_bitmap[bm_idx/8] & (1 << (bm_idx&7)));
  f_block_bitmap[bm_idx/8] ^= (1 << (bm_idx&7));
  
  full_pwrite(gfd, f_block_bitmap, sb.block_size, bm_off);
  
  ++bgi[idx].free_blocks;
  ++sb.free_blocks;
  
  pthread_mutex_unlock(&blocks_io_lock);
}


uint32_t new_ino()
{
  static int idx = 0;
  static int bm_idx = 0;
  int bit_idx;
  
  uint64_t bm_off;
  uint32_t ret;
  
  pthread_mutex_lock(&inodes_io_lock);
  
  if(sb.free_inodes == 0) {
    pthread_mutex_unlock(&inodes_io_lock);
    return 0;
  }
  
  while(bgi[idx].free_inodes == 0) {
    ++idx;
    if(idx == sb.max_bgroup)
      idx = 0;
      
    bm_idx = 0;
  }
  
  bm_off = sb.bgroups_start + (uint64_t)idx * sb.bgroup_size;
  
  full_pread(gfd, inode_bitmap, sb.block_size, bm_off);
  
  while(inode_bitmap[bm_idx] == 0xFF) {
    ++bm_idx;
    if(bm_idx == block_size)
      bm_idx = 0;
  }
    
  bit_idx = 0;
  while(inode_bitmap[bm_idx] & (1 << bit_idx))
    ++bit_idx;
    
  inode_bitmap[bm_idx] |= (1 << bit_idx);
  
  full_pwrite(gfd, inode_bitmap, sb.block_size, bm_off);
  
  ret = idx * 8 * block_size + (8*bm_idx+bit_idx);
  
  --bgi[idx].free_inodes;
  --sb.free_inodes;
  
  pthread_mutex_unlock(&inodes_io_lock);
  
  return ret+1;
}

void free_ino(uint32_t ino)
{
  uint32_t idx;
  uint64_t bm_off;
  uint32_t bm_idx; 
  
  --ino;
  idx = ino / (8 * sb.block_size);
  bm_off = sb.bgroups_start + (uint64_t)idx * sb.bgroup_size;
  bm_idx = ino & (8 * sb.block_size - 1);

  pthread_mutex_lock(&inodes_io_lock);

  full_pread(gfd, f_inode_bitmap, sb.block_size, bm_off);
  
  assert( f_inode_bitmap[bm_idx/8] & (1 << (bm_idx&7)) );
  f_inode_bitmap[bm_idx/8] ^= (1 << (bm_idx&7));
  
  full_pwrite(gfd, f_inode_bitmap, sb.block_size, bm_off);
  
  ++bgi[idx].free_inodes;
  ++sb.free_inodes;
  
  pthread_mutex_unlock(&inodes_io_lock);
}

static inline
void read_block(void* buf, uint32_t bidx, uint64_t off, uint64_t count)
{
  assert(off + count <= block_size);
  full_pread(gfd, buf, count, (uint64_t)bidx * block_size + off); 
}  

static inline
void write_block(void* buf, uint32_t bidx, uint64_t off, uint64_t count)
{
  assert(off + count <= block_size);
  full_pwrite(gfd, buf, count, (uint64_t)bidx * block_size + off); 
}

uint32_t new_block_zero()
{  
  uint32_t bidx = new_block();
  if(bidx == 0)
    return 0;
  
  for(uint32_t offset = 0; offset < block_size; offset += PG_SIZE)
    write_block(zero_page, bidx, offset, min(PG_SIZE, (block_size - offset)) ); 
  
  return bidx;
}

void read_inode(struct sfs_inode* inode)
{
  uint32_t ino = inode->ino - 1;
  uint32_t idx = ino / (8 * sb.block_size);
  uint64_t inode_table_off = sb.bgroups_start + (uint64_t)idx * sb.bgroup_size + 2 * sb.block_size;
  ino &= (8 * sb.block_size - 1);
  
  full_pread(gfd, inode, INODE_DISK_SIZE, inode_table_off + ino * INODE_DISK_SIZE);
}

void write_inode(struct sfs_inode* inode)
{
  uint32_t ino = inode->ino - 1;
  uint32_t idx = ino / (8 * sb.block_size);
  uint64_t inode_table_off = sb.bgroups_start + (uint64_t)idx * sb.bgroup_size + 2 * sb.block_size;
  ino &= (8 * sb.block_size - 1);
  
  full_pwrite(gfd, inode, INODE_DISK_SIZE, inode_table_off + ino * INODE_DISK_SIZE);
}


#define MAX_INODE_LISTS 4096

struct sfs_inode* inode_list[MAX_INODE_LISTS];
pthread_rwlock_t inode_list_lock[MAX_INODE_LISTS];

void init_icache()
{
  for(size_t i = 0; i < MAX_INODE_LISTS; ++i) {
    pthread_rwlock_init(&inode_list_lock[i], NULL);
  }
}

struct sfs_inode* list_find_inode(struct sfs_inode* list, uint32_t ino)
{
  while(list) {
    if(list->ino == ino)
      return list;
    
    list = list->next;
  }
  
  return NULL;
}

void list_add_inode(uint32_t idx, struct sfs_inode* inode)
{
  dmsg("adding ino = %u to icache", inode->ino);
  
  inode->next = inode_list[idx];
  inode_list[idx] = inode;
}

struct sfs_inode* get_inode(uint32_t ino)
{
  struct sfs_inode* inode;
  uint32_t idx = ino & (MAX_INODE_LISTS-1);
  
  pthread_rwlock_rdlock(&inode_list_lock[idx]);
  inode = list_find_inode(inode_list[idx], ino);
  pthread_rwlock_unlock(&inode_list_lock[idx]); 

  if(inode != NULL)
    return inode;
  
  
  pthread_rwlock_wrlock(&inode_list_lock[idx]);
  
  inode = list_find_inode(inode_list[idx], ino);
  
  if(inode == NULL) {
    inode = malloc(sizeof(struct sfs_inode));
    
    inode->ino = ino;
    pthread_rwlock_init(&inode->lock, NULL);
    pthread_rwlock_init(&inode->bptr_lock, NULL);
    inode->version = 0;
    
    read_inode(inode);
    
    list_add_inode(idx, inode);
  }

  pthread_rwlock_unlock(&inode_list_lock[idx]);

  return inode;
}

struct sfs_inode* remove_inode(uint32_t ino)
{
  struct sfs_inode* inode;
  struct sfs_inode* ret;
  uint32_t idx = ino & (MAX_INODE_LISTS-1);
  
  pthread_rwlock_wrlock(&inode_list_lock[idx]);

  inode = inode_list[idx];
  assert(inode);
  
  if(inode->ino == ino) {
    ret = inode;
    inode_list[idx] = inode->next;
    goto out;
  }
  
  while(1) {
    assert(inode->next);
    
    if(inode->next->ino == ino) {
      ret = inode->next;
      inode->next = ret->next;
      goto out;
    }
    
    inode = inode->next;
  }

out:
  pthread_rwlock_unlock(&inode_list_lock[idx]); 
  return ret;
}

struct sfs_inode* new_inode()
{
  uint32_t ino;
  struct sfs_inode* inode;
  uint32_t idx;

  ino = new_ino();
  
  dmsg("new inode() ino = %u", ino);

  inode = malloc(sizeof(struct sfs_inode));
  memset(inode, 0, INODE_DISK_SIZE);
  
  inode->atime = time(NULL);
  inode->mtime = time(NULL);
  inode->ctime = time(NULL);
  
  inode->ino = ino;
  pthread_rwlock_init(&inode->lock, NULL);
  pthread_rwlock_init(&inode->bptr_lock, NULL);
  inode->version = 0;
    
  idx = inode->ino & (MAX_INODE_LISTS-1);
 
  pthread_rwlock_wrlock(&inode_list_lock[idx]);
  list_add_inode(idx, inode);
  pthread_rwlock_unlock(&inode_list_lock[idx]);

  return inode;
}

static inline void inode_rdlock(struct sfs_inode* inode)
{
  pthread_rwlock_rdlock(&inode->lock);
}

static inline void inode_wrlock(struct sfs_inode* inode)
{
  pthread_rwlock_wrlock(&inode->lock);
}

static inline void inode_unlock(struct sfs_inode* inode)
{
  pthread_rwlock_unlock(&inode->lock);
}

static inline void inode_data_rdlock(struct sfs_inode* inode)
{
  pthread_rwlock_rdlock(&inode->bptr_lock);
}

static inline void inode_data_wrlock(struct sfs_inode* inode)
{
  pthread_rwlock_wrlock(&inode->bptr_lock);
}

static inline void inode_data_unlock(struct sfs_inode* inode)
{
  pthread_rwlock_unlock(&inode->bptr_lock);
}


static inline void inode_inc_nlink(struct sfs_inode* inode)
{
  inode_wrlock(inode);
  ++inode->nlink;
  inode_unlock(inode);
} 

static inline void inode_dec_nlink(struct sfs_inode* inode)
{
  inode_wrlock(inode);
  --inode->nlink;
  inode_unlock(inode);
} 


static inline void inode_set_atime_now(struct sfs_inode* inode)
{
  inode_wrlock(inode);
  inode->atime = time(NULL);
  inode_unlock(inode);
}

static inline void inode_set_mtime_now(struct sfs_inode* inode)
{
  inode_wrlock(inode);
  inode->mtime = time(NULL);
  inode_unlock(inode);
}

static inline void inode_set_ctime_now(struct sfs_inode* inode)
{
  inode_wrlock(inode);
  inode->ctime = time(NULL);
  inode_unlock(inode);
}


static inline void inode_inc_blocks(struct sfs_inode* inode)
{
  inode_wrlock(inode);
  ++inode->blocks;
  inode_unlock(inode);
}

static inline uint64_t inode_get_size(struct sfs_inode* inode)
{
  uint64_t ret;
  
  inode_rdlock(inode);
  ret = inode->size;
  inode_unlock(inode);
  
  return ret;
}


static inline 
void read_inode_data_l0(uint32_t bidx, void* buf, uint64_t off, uint64_t count)
{
  if(bidx == 0) {
    memset(buf, 0, count);
    return;
  }
  
  read_block(buf, bidx, off, count);
}

static inline
void read_inode_data_l1(uint32_t bidx, void* buf, uint64_t off, uint64_t count)
{
  if(bidx == 0) {
    memset(buf, 0, count);
    return;
  }
  
  read_block(&bidx, bidx, (off / block_size) * 4, 4);
  read_inode_data_l0(bidx, buf, (off & block_mask), count);
}

static inline
void read_inode_data_l2(uint32_t bidx, void* buf, uint64_t off, uint64_t count)
{  
  if(bidx == 0) {
    memset(buf, 0, count);
    return;
  }
  
  read_block(&bidx, bidx, (off / bptr1_cap) * 4, 4);
  read_inode_data_l1(bidx, buf, (off & (bptr1_cap-1)), count);
}

static inline
void read_inode_data_l3(uint32_t bidx, void* buf, uint64_t off, uint64_t count)
{  
  if(bidx == 0) {
    memset(buf, 0, count);
    return;
  }
  
  read_block(&bidx, bidx, (off / bptr2_cap) * 4, 4);
  read_inode_data_l2(bidx, buf, (off & (bptr2_cap-1)), count);
}

void read_inode_data(struct sfs_inode* inode, void* buf, uint64_t offset, uint64_t count)
{
  while(count > 0) {
    uint64_t cur_count = min(count, block_size - (offset & block_mask));
    
    if(offset < bptr0_lim) {
      int idx = offset / block_size;
      read_block(buf, inode->bptr[idx], (offset & block_mask), cur_count);
    } else if(offset < bptr1_lim) {
      read_inode_data_l1(inode->bptr[N_BPTR0], buf, (offset - bptr0_lim), cur_count);
    } else if(offset < bptr2_lim) {
      read_inode_data_l2(inode->bptr[N_BPTR0+1], buf, (offset - bptr1_lim), cur_count);
    } else {
      read_inode_data_l3(inode->bptr[N_BPTR0+2], buf, (offset - bptr2_lim), cur_count);
    }

    buf = (char*)buf + cur_count;
    count -= cur_count;
    offset += cur_count;
  } 
}



static inline
uint32_t inode_alloc_block(struct sfs_inode* inode, bool* wrlock, int idx)
{
  if(inode->bptr[idx])
    return inode->bptr[idx];
  
  if(!*wrlock) {
    *wrlock = true;
    inode_data_unlock(inode);
    inode_data_wrlock(inode);
                      
    if(inode->bptr[idx])
      return inode->bptr[idx];
  }
  
  if(idx < N_BPTR0) {
    inode->bptr[idx] = new_block();

    if(inode->bptr[idx])
      inode_inc_blocks(inode);
  } else {  
    inode->bptr[idx] = new_block_zero();
  }
  

  
  return inode->bptr[idx];
}

static inline
uint32_t inode_get_next_block(struct sfs_inode* inode, bool* wrlock,
        uint32_t bidx, uint64_t off, uint64_t bptr_size, bool inc_blocks)
{
  uint32_t next_bidx;
  int idx = (off / bptr_size) * 4;
  
  read_block(&next_bidx, bidx, idx, 4);
  
  if(next_bidx)
    return next_bidx;

  if(!*wrlock) {
    *wrlock = true;
    inode_data_unlock(inode);
    inode_data_wrlock(inode);
      
    read_block(&next_bidx, bidx, idx, 4);
    if(next_bidx)
      return next_bidx;
  }
  
  next_bidx = new_block_zero();
  if(next_bidx == 0)
    return 0;
        
  write_block(&next_bidx, bidx, idx, 4);  
  
  if(inc_blocks)
    inode_inc_blocks(inode);
  
  return next_bidx;
}

uint64_t write_inode_data(struct sfs_inode* inode, void* buf, uint64_t offset, uint64_t count, bool wrlock)
{
  uint64_t ret = 0;
    
  while(count > 0) {
    uint64_t cur_count = min(count, block_size - (offset & block_mask));

    if(offset < bptr0_lim) {
      int idx = offset / block_size;
      uint64_t off = (offset & block_mask);
      uint32_t bidx;
    
      if((bidx = inode_alloc_block(inode, &wrlock, idx)) == 0)
        break;
      
      write_block(buf, bidx, off, cur_count);

    } else if(offset < bptr1_lim) {
      int idx = N_BPTR0;
      uint64_t off = offset - bptr0_lim;
      uint32_t bidx;
            
      if((bidx = inode_alloc_block(inode, &wrlock, idx)) == 0)
        break;
      if((bidx = inode_get_next_block(inode, &wrlock, bidx, off, block_size, true)) == 0)
        break;
        
      write_block(buf, bidx, (off & block_mask), cur_count);
    } else if(offset < bptr2_lim) {
      int idx = N_BPTR0 + 1;
      uint64_t off = offset - bptr1_lim;
      uint32_t bidx;
            
      if((bidx = inode_alloc_block(inode, &wrlock, idx)) == 0)
        break;
      if((bidx = inode_get_next_block(inode, &wrlock, bidx, off, bptr1_cap, false)) == 0)
        break;
      if((bidx = inode_get_next_block(inode, &wrlock, bidx, (off & (bptr1_cap-1)), block_size, true)) == 0)
        break;
        
      write_block(buf, bidx, (off & block_mask), cur_count);
    } else {
      int idx = N_BPTR0 + 2;
      uint64_t off = offset - bptr2_lim;
      uint32_t bidx;
      
      if((bidx = inode_alloc_block(inode, &wrlock, idx)) == 0)
        break;
      if((bidx = inode_get_next_block(inode, &wrlock, bidx, off, bptr2_cap, false)) == 0)
        break;
      if((bidx = inode_get_next_block(inode, &wrlock, bidx, (off & (bptr2_cap-1)), bptr1_cap, false)) == 0)
        break;
      if((bidx = inode_get_next_block(inode, &wrlock, bidx, (off & (bptr1_cap-1)), block_size, true)) == 0)
        break;
        
      write_block(buf, bidx, (off & block_mask), cur_count);
    }
    
    buf = (char*)buf + cur_count;
    count -= cur_count;
    offset += cur_count;
    ret += cur_count;
  }
    
  return ret;
}



uint32_t inode_truncate_l1(uint32_t bidx, uint64_t start, uint64_t size)
{
  uint32_t idx;
  uint32_t ret = 0;
  uint32_t* bt = malloc(block_size);
  
  if(size <= start) {
    idx = 0;
  } else {
    idx = (size - start - 1) / block_size + 1;
  }
  
  read_block(bt, bidx, 0, block_size);
  
  for(uint32_t i = idx; i < block_size/4; ++i) {
    if(bt[i]) {
      free_block(bt[i]);
      bt[i] = 0;
      ++ret;
    }
  }
  
  write_block(bt, bidx, 0, block_size);
  free(bt);
  
  return ret;
}

uint32_t inode_truncate_l2(uint32_t bidx, uint64_t start, uint64_t size)
{
  uint32_t idx;
  uint32_t ret = 0;
  uint32_t* bt = malloc(block_size);
  
  if(size <= start) {
    idx = 0;
  } else {
    idx = (size - start) / bptr1_cap;
  }
  
  read_block(bt, bidx, 0, block_size);
  
  start += idx*bptr1_cap;
  if(bt[idx]) {
    ret += inode_truncate_l1(bt[idx], start, size);
  }
  
  if(size == start && bt[idx]) {
    free_block(bt[idx]);
    bt[idx] = 0;
  }
  
  start += bptr1_cap;
  ++idx;
  
  while(idx < (block_size / 4)) {
    if(bt[idx]) {
      ret += inode_truncate_l1(bt[idx], start, size);
      free_block(bt[idx]);
      bt[idx] = 0;
    }
    
    start += bptr1_cap;
    ++idx;
  }
  
  write_block(bt, bidx, 0, block_size);
  free(bt);
  
  return ret;
}

uint32_t inode_truncate_l3(uint32_t bidx, uint64_t start, uint64_t size)
{
  uint32_t idx;
  uint32_t ret = 0;
  uint32_t* bt = malloc(block_size);
  
  if(size <= start) {
    idx = 0;
  } else {
    idx = (size - start) / bptr2_cap;
  }
  
  read_block(bt, bidx, 0, block_size);
  
  start += idx*bptr2_cap;
  if(bt[idx]) {  
    ret += inode_truncate_l2(bt[idx], start, size);
  }
  
  if(size == start && bt[idx]) {
    free_block(bt[idx]);
    bt[idx] = 0;
  }
  
  start += bptr2_cap;
  ++idx;
  
  while(idx < (block_size / 4)) {
    if(bt[idx]) {
      ret += inode_truncate_l2(bt[idx], start, size);
      free_block(bt[idx]);
      bt[idx] = 0;
    }
    
    start += bptr2_cap;
    ++idx;
  }
  
  write_block(bt, bidx, 0, block_size);
  free(bt);
  
  return ret;
}

void inode_truncate(struct sfs_inode* inode, uint64_t size)
{
  uint64_t old_size;
  int idx;
  uint32_t block;
  uint32_t freed_blocks = 0;
  
  inode_wrlock(inode);
  old_size = inode->size;
  inode->size = size;
  
  inode->mtime = time(NULL);
  inode_unlock(inode);
  
  if(old_size <= size)
    return;
  
  
  for(idx = 0; idx < N_BPTR0; ++idx) {
    if(size <= idx*block_size && inode->bptr[idx]) {
      free_block(inode->bptr[idx]);
      inode->bptr[idx] = 0;
      ++freed_blocks;
    }
  }
  
  
  block = inode->bptr[idx];
  if(block && size < bptr1_lim) {
    freed_blocks += inode_truncate_l1(block, bptr0_lim, size);
    
    if(size <= bptr0_lim) {
      free_block(block);
      inode->bptr[idx] = 0;
    }
  }
  
  
  ++idx;
  block = inode->bptr[idx];
  if(block && size < bptr2_lim) {
    freed_blocks += inode_truncate_l2(block, bptr1_lim, size);
    
    if(size <= bptr1_lim) {
      free_block(block);
      inode->bptr[idx] = 0;
    }
  }
  
  
  ++idx;
  block = inode->bptr[idx];
  if(inode->bptr[idx]) {
    freed_blocks += inode_truncate_l3(block, bptr2_lim, size);
    
    if(size <= bptr2_lim) {
      free_block(block);
      inode->bptr[idx] = 0;
    }  
  }
  
  
  inode_wrlock(inode);
  inode->blocks -= freed_blocks;
  inode_unlock(inode);
}



static void sfs_forget(fuse_req_t req, fuse_ino_t ino, uint64_t nlookup)
{
  struct sfs_inode* inode;
  
  dmsg("sfs_forget() ino = %u", (uint32_t)ino);
  
  inode = remove_inode(ino);
  
  inode_rdlock(inode);
  
  if(inode->nlink == 0 || (inode->nlink == 1 && (inode->mode & S_IFDIR))) {
    dmsg("deleting from disk ino = %u", (uint32_t)ino);
    
    inode_unlock(inode);
    
    inode_data_wrlock(inode);
    inode_truncate(inode, 0);
    inode_data_unlock(inode);
    
    free_ino(ino);
  } else {
    inode_unlock(inode);
    write_inode(inode);
  }
  
  free(inode);
}


static void fill_attr(struct sfs_inode* inode, struct stat* stbuf)
{
  memset(stbuf, 0, sizeof(struct stat));
  
  stbuf->st_ino = inode->ino;
  stbuf->st_mode = inode->mode;
  stbuf->st_nlink = inode->nlink;
  stbuf->st_uid = inode->uid;
  stbuf->st_gid = inode->gid;
  stbuf->st_size = inode->size;
  stbuf->st_atime = inode->atime;
  stbuf->st_mtime = inode->mtime;
  stbuf->st_ctime = inode->ctime;
  stbuf->st_blksize = sb.block_size;
  stbuf->st_blocks = inode->blocks * (block_size / 512);
}

static void fill_fuse_entry_param(struct sfs_inode* inode, struct fuse_entry_param* e)
{
  e->ino = inode->ino;
  e->generation = 1;
  e->attr_timeout = NO_TIMEOUT;
  e->entry_timeout = NO_TIMEOUT;
  
  fill_attr(inode, &e->attr);
}


static void sfs_getattr(fuse_req_t req, fuse_ino_t ino,
			     struct fuse_file_info *fi)
{
  struct sfs_inode* inode;
	struct stat stbuf;

  dmsg("sfs_getattr() ino = %u", (uint32_t)ino);

  inode = get_inode(ino);
  
  inode_rdlock(inode);
  fill_attr(inode, &stbuf);
  inode_unlock(inode);
  
  fuse_reply_attr(req, &stbuf, NO_TIMEOUT);
}

static void sfs_setattr(fuse_req_t req, fuse_ino_t ino, struct stat *attr, 
           int to_set, struct fuse_file_info *fi)
{
  struct sfs_inode* inode;
  struct stat stbuf;
  
  dmsg("sfs_setattr() ino = %u to_set = %d", (uint32_t)ino, to_set);
  
  inode = get_inode(ino);
  
  inode_wrlock(inode);
  
  inode->ctime = time(NULL);
  
  if(to_set & FUSE_SET_ATTR_MODE) {
    inode->mode = attr->st_mode;
  } 
  
  if(to_set & FUSE_SET_ATTR_UID) {
    inode->uid = attr->st_uid;
  }
  
  if(to_set & FUSE_SET_ATTR_GID) {
    inode->gid = attr->st_gid;
  }
   
  if(to_set & FUSE_SET_ATTR_SIZE) {
    inode_unlock(inode);
    
    inode_data_wrlock(inode);
    inode_truncate(inode, attr->st_size);
    inode_data_unlock(inode);
    
    inode_wrlock(inode);
  }
  
  if(to_set & FUSE_SET_ATTR_ATIME) {
    inode->atime = stbuf.st_atime;
  }
  
  if(to_set & FUSE_SET_ATTR_MTIME) {
    inode->mtime = stbuf.st_mtime;
  }
  
  if(to_set & FUSE_SET_ATTR_CTIME) {
    inode->ctime = stbuf.st_ctime;
  }
  
  if(to_set & FUSE_SET_ATTR_ATIME_NOW) {
    inode->atime = time(NULL);
  }
  
  if(to_set & FUSE_SET_ATTR_MTIME_NOW) {
    inode->mtime = time(NULL);
  }

  fill_attr(inode, &stbuf);
  inode_unlock(inode);

  fuse_reply_attr(req, &stbuf, NO_TIMEOUT);
}


static inline void inode_inc_version(struct sfs_inode* dir_inode)
{
  inode_wrlock(dir_inode);
  ++dir_inode->version;
  inode_unlock(dir_inode);
}

static inline uint64_t inode_get_version(struct sfs_inode* dir_inode)
{
  uint64_t ret;
  
  inode_rdlock(dir_inode);
  ret = dir_inode->version;
  inode_unlock(dir_inode);
  
  return ret;
}

struct opendir_descriptor {
  struct sfs_inode* inode;
  uint64_t version;
  uint32_t last_ino;
};

static void sfs_opendir(fuse_req_t req, fuse_ino_t ino, struct fuse_file_info *fi)
{
  struct opendir_descriptor* desc;
  struct sfs_inode* inode;
  
  dmsg("sfs_opendir() ino = %u", (uint32_t)ino);
  
  inode = get_inode(ino);

  desc = malloc(sizeof(struct opendir_descriptor));
  desc->inode = inode;
  desc->last_ino = 0;
  desc->version = inode_get_version(inode);
  fi->fh = (uint64_t)desc;
  
  fuse_reply_open(req, fi);
}

static void sfs_releasedir(fuse_req_t req, fuse_ino_t ino, struct fuse_file_info *fi)
{
  free( (struct opendir_descriptor*)fi->fh );
  fuse_reply_err(req, 0);
}

static void sfs_readdir(fuse_req_t req, fuse_ino_t ino, size_t size,
			     off_t off, struct fuse_file_info *fi)
{
  struct opendir_descriptor* desc;
  struct sfs_inode* inode;
  char dir_data[PG_SIZE];
  char buf[PG_SIZE];
  char name[MAX_NAME_LEN+1];
  uint64_t dir_size;
  size_t data_len;
  size_t buf_size;
  
  dmsg("sfs_readdir() ino = %u", (uint32_t)ino);

  desc = (struct opendir_descriptor*)fi->fh;
  inode = desc->inode;
  dir_size = inode_get_size(inode);
  
  inode_set_atime_now(inode);
  
  inode_data_rdlock(inode);
  
  if(desc->version != inode_get_version(inode)) {
    char* buf;
    
    buf = malloc(dir_size);
    if(buf == NULL) {
      inode_data_unlock(inode);
      fuse_reply_err(req, ENOMEM);
      return;
    }
    
    for(off = 0; off < dir_size; ) {
      struct sfs_dentry* dentry = (struct sfs_dentry*)(buf + off);
      if(dentry->ino > desc->last_ino)
        break;
      
      off += DENTRY_SIZE(dentry);
    }
    
    free(buf);
  }

  if(size > PG_SIZE)
    size = PG_SIZE;
  buf_size = 0;

  data_len = min(PG_SIZE, dir_size - off); 
  
  read_inode_data(inode, dir_data, off, data_len);
  inode_data_unlock(inode);
  
  for(size_t data_off = 0; data_off < data_len; ) {
    struct sfs_dentry* dentry;
    uint32_t dentry_size;
    struct stat stbuf;
    size_t ret;
    
    if(data_off + sizeof(struct sfs_dentry) > data_len)
      break;
    
    dentry = (struct sfs_dentry*)(dir_data + data_off);
    dentry_size = DENTRY_SIZE(dentry);
    
    if(data_off + dentry_size > data_len)
      break;

    data_off += dentry_size;


    memcpy(name, dentry->name, dentry->name_len);
    name[dentry->name_len] = 0;
    
    memset(&stbuf, 0, sizeof(stbuf));
    stbuf.st_ino = dentry->ino;
    stbuf.st_mode = dentry->type<<12;
    
    ret = fuse_add_direntry(req, buf+buf_size, size-buf_size, name, &stbuf, off+data_off);
    
    if(buf_size + ret > size)
      break;
    
    desc->last_ino = dentry->ino;
    buf_size += ret;
  }

  fuse_reply_buf(req, buf, buf_size);
}

static int sfs_add_dentry(struct sfs_inode* dir_inode, uint32_t ino, uint8_t type, uint8_t name_len, const char* name)
{
  uint64_t dir_size;
  uint64_t dentry_size;
  uint64_t off;
  struct sfs_dentry dentry;
  char* buf;
  
  dentry.ino = ino;
  dentry.type = type;
  dentry.name_len = name_len;
  dentry_size = DENTRY_SIZE(&dentry);
  
  dir_size = inode_get_size(dir_inode);
  
  buf = malloc(dir_size + dentry_size);
  if(buf == NULL) {
    return ENOMEM;
  }
  
  read_inode_data(dir_inode, buf, 0, dir_size);
    
  if(write_inode_data(dir_inode, zero_page, dir_size, dentry_size, true) < dentry_size) {
    free(buf);
    return EDQUOT;
  }
      
  for(off = 0; off < dir_size; ) {
    struct sfs_dentry* p_dentry;
      
    p_dentry = (struct sfs_dentry*)(buf + off);
    if(p_dentry->ino > ino)
      break;
      
    off += DENTRY_SIZE(p_dentry);
  }
    
  memcpy(buf+off+dentry_size, buf+off, dir_size-off);
  memcpy(buf+off, &dentry, sizeof(struct sfs_dentry));
  memcpy(buf+off+sizeof(struct sfs_dentry), name, name_len);
  
  write_inode_data(dir_inode, buf, 0, dir_size+dentry_size, true);
  
  inode_wrlock(dir_inode);
  dir_inode->size += dentry_size;
  inode_unlock(dir_inode);
  
  inode_inc_version(dir_inode);
  inode_set_mtime_now(dir_inode);
  
  free(buf);
  return 0;
}

struct sfs_dentry* sfs_find_dentry(char* buf, uint64_t buf_len, const char* name, uint8_t name_len)
{
  for(uint64_t off = 0; off < buf_len; ) {
    struct sfs_dentry* dentry;
      
    dentry = (struct sfs_dentry*)(buf + off);
    
    if(name_len == dentry->name_len) {
      if(!strncmp(name, dentry->name, name_len)) {
        return dentry;
      }
    }
      
    off += DENTRY_SIZE(dentry);
  }
  
  return NULL;
}

void sfs_remove_dentry(struct sfs_inode* dir_inode, char* buf, uint64_t dir_size, struct sfs_dentry* dentry)
{
  int dentry_size = DENTRY_SIZE(dentry);
  char* dentry_end = (char*)dentry + dentry_size;
  
  memmove((char*)dentry, dentry_end, buf + dir_size - dentry_end);
  write_inode_data(dir_inode, buf, 0, dir_size - dentry_size, true);
  
  
  inode_wrlock(dir_inode);
  dir_inode->size -= dentry_size;
  inode_unlock(dir_inode);

  inode_inc_version(dir_inode);
  inode_set_mtime_now(dir_inode);
}

static void sfs_lookup(fuse_req_t req, fuse_ino_t dir_ino, const char* name)
{
  struct sfs_inode* dir_inode;
  struct sfs_inode* inode;
  struct sfs_dentry* dentry;
  struct fuse_entry_param e;
  char* buf;
  uint64_t name_len;
  uint64_t ino;
  uint64_t dir_size;

  dmsg("sfs_lookup() ino = %u name = %s", (uint32_t)dir_ino, name);

  name_len = strlen(name);
  
  if(name_len > MAX_NAME_LEN) {
    fuse_reply_err(req, ENAMETOOLONG);
    return;
  }

  dir_inode = get_inode(dir_ino);
  dir_size = inode_get_size(dir_inode);
  
  buf = malloc(dir_size);
  if(!buf) {
    fuse_reply_err(req, ENOMEM);
    return;
  }
  
  inode_set_atime_now(dir_inode);
  
  inode_data_rdlock(dir_inode);
  read_inode_data(dir_inode, buf, 0, dir_size);
  inode_data_unlock(dir_inode);


  dentry = sfs_find_dentry(buf, dir_inode->size, name, name_len);
  if(dentry == NULL) {
    fuse_reply_err(req, ENOENT);
    free(buf);
    return;
  }
  
  ino = dentry->ino;
  free(buf);
  
  inode = get_inode(ino);
  fill_fuse_entry_param(inode, &e);
  fuse_reply_entry(req, &e);
}

void sfs_link(fuse_req_t req, fuse_ino_t ino, fuse_ino_t dir_ino, const char *name)
{
  struct sfs_inode* dir_inode;
  struct sfs_inode* inode;
  struct fuse_entry_param e;
  uint64_t name_len;
  
  dmsg("sfs_link() ino = %u name = %s\n", (uint32_t)dir_ino, name);
  
  name_len = strlen(name);
  if(name_len > MAX_NAME_LEN) {
    fuse_reply_err(req, ENAMETOOLONG);
    return;
  }
  
  dir_inode = get_inode(dir_ino);
  inode = get_inode(ino);
  
  inode_data_wrlock(dir_inode);
  sfs_add_dentry(dir_inode, ino, DENTRY_TYPE(inode->mode), name_len, name);
  inode_data_unlock(dir_inode);
    
  inode_inc_nlink(inode);
  inode_set_ctime_now(inode);
  
  fill_fuse_entry_param(inode, &e);
  fuse_reply_entry(req, &e);
}

static void sfs_create(fuse_req_t req, fuse_ino_t dir_ino, const char *name, mode_t mode, struct fuse_file_info *fi)
{
  struct sfs_inode* dir_inode;
  struct sfs_inode* inode;
  const struct fuse_ctx* ctx;
  struct fuse_entry_param e;
  uint64_t name_len;
  
  dmsg("sfs_create() ino = %u name = %s", (uint32_t)dir_ino, name);
  
  name_len = strlen(name);
  if(name_len > MAX_NAME_LEN) {
    fuse_reply_err(req, ENAMETOOLONG);
    return;
  }
  
  ctx = fuse_req_ctx(req);
  
  inode = new_inode();
  inode->mode = S_IFREG | mode;
  inode->nlink = 1;
  inode->uid = ctx->uid;
  inode->gid = ctx->gid;
  
  dir_inode = get_inode(dir_ino);
  inode_set_mtime_now(dir_inode);
  
  inode_data_wrlock(dir_inode);
  sfs_add_dentry(dir_inode, inode->ino, DENTRY_TYPE_REGFILE, name_len, name);
  inode_data_unlock(dir_inode);
    
  fill_fuse_entry_param(inode, &e);
  
  fi->fh = (uint64_t)inode;
  
  fuse_reply_create(req, &e, fi);
}

void sfs_unlink(fuse_req_t req, fuse_ino_t dir_ino, const char *name)
{
  struct sfs_inode* dir_inode;
  struct sfs_inode* inode;
  struct sfs_dentry* dentry;
  uint64_t name_len;
  uint64_t dir_size;
  uint64_t ino;
  char* buf;
  
  dmsg("sfs_unlink() ino = %u name = %s", (uint32_t)dir_ino, name);
  
  name_len = strlen(name);
  if(name_len > MAX_NAME_LEN) {
    fuse_reply_err(req, ENAMETOOLONG);
    return;
  }
  
  dir_inode = get_inode(dir_ino);
  dir_size = inode_get_size(dir_inode);
  
  buf = malloc(dir_size);
  if(!buf) {
    fuse_reply_err(req, ENOMEM);
    return;
  }
  
  inode_data_wrlock(dir_inode);
  read_inode_data(dir_inode, buf, 0, dir_size);
  
  dentry = sfs_find_dentry(buf, dir_size, name, name_len);
  assert(dentry);
    
  ino = dentry->ino;
  inode = get_inode(ino);
    
  sfs_remove_dentry(dir_inode, buf, dir_size, dentry);
  
  inode_data_unlock(dir_inode);
  free(buf);
  
  inode_dec_nlink(inode);
  inode_set_ctime_now(inode);
  
  fuse_reply_err(req, 0);
}


void sfs_mkdir(fuse_req_t req, fuse_ino_t dir_ino, const char *name, mode_t mode)
{
  struct sfs_inode* dir_inode;
  struct sfs_inode* inode;
  const struct fuse_ctx* ctx;
  struct fuse_entry_param e;
  uint64_t name_len;
  
  dmsg("sfs_mkdir() ino = %u name = %s", (uint32_t)dir_ino, name);
  
  name_len = strlen(name);
  if(name_len > MAX_NAME_LEN) {
    fuse_reply_err(req, ENAMETOOLONG);
    return;
  }
  
  ctx = fuse_req_ctx(req);
  
  inode = new_inode();
  inode->mode = S_IFDIR | mode;
  inode->nlink = 2;
  inode->uid = ctx->uid;
  inode->gid = ctx->gid;
    
  dir_inode = get_inode(dir_ino);
    
  inode_data_wrlock(dir_inode);
  sfs_add_dentry(dir_inode, inode->ino, DENTRY_TYPE_DIR, name_len, name);
  inode_data_unlock(dir_inode);
    
  inode_data_wrlock(inode);
  sfs_add_dentry(inode, inode->ino, DENTRY_TYPE_DIR, 1, ".");
  sfs_add_dentry(inode, dir_inode->ino, DENTRY_TYPE_DIR, 2, "..");
  inode_data_unlock(inode);
  
  inode_inc_nlink(dir_inode);
  inode_set_ctime_now(dir_inode);
  
  fill_fuse_entry_param(inode, &e);
  fuse_reply_entry(req, &e);
}

void sfs_rmdir(fuse_req_t req, fuse_ino_t dir_ino, const char *name)
{
  struct sfs_inode* dir_inode;
  struct sfs_inode* inode;
  struct sfs_dentry* dentry;
  uint64_t name_len;
  uint64_t dir_size;
  uint64_t ino;
  char* buf;
  
  dmsg("sfs_rmdir() ino = %u name = %s", (uint32_t)dir_ino, name);
  
  name_len = strlen(name);
  if(name_len > MAX_NAME_LEN) {
    fuse_reply_err(req, ENAMETOOLONG);
    return;
  }
  
  dir_inode = get_inode(dir_ino);
  dir_size = inode_get_size(dir_inode);
  
  if(dir_size  == EMPTRY_DIR_SIZE) {
    fuse_reply_err(req, ENOTEMPTY);
    return;
  }
  
  buf = malloc(dir_size);
  if(!buf) {
    fuse_reply_err(req, ENOMEM);
    return;
  }
  
  inode_data_wrlock(dir_inode);
  read_inode_data(dir_inode, buf, 0, dir_size);
  
  dentry = sfs_find_dentry(buf, dir_size, name, name_len);
  assert(dentry);
    
  ino = dentry->ino;
  inode = get_inode(ino);
    
  sfs_remove_dentry(dir_inode, buf, dir_size, dentry);
  
  inode_data_unlock(dir_inode);
  free(buf);
  
  inode_dec_nlink(dir_inode);
  inode_dec_nlink(inode);
  inode_set_ctime_now(inode);
  
  fuse_reply_err(req, 0);
}

static void sfs_rename(fuse_req_t req, fuse_ino_t parent, const char* name, 
           fuse_ino_t newparent, const char* newname, unsigned int flags)
{
  struct sfs_inode* dir_inode;
  struct sfs_inode* newdir_inode;
  struct sfs_dentry* dentry;
  struct sfs_dentry t_dentry;
  uint64_t dir_size;
  uint64_t newdir_size;
  uint64_t name_len;
  char* d_buf;
  char* nd_buf;
  
  dmsg("sfs_rename() name = %s newname = %s", name, newname);
  
  name_len = max(strlen(name), strlen(newname));
  if(name_len > MAX_NAME_LEN) {
    fuse_reply_err(req, ENAMETOOLONG);
    return;
  }
  
  if(flags) {
    fuse_reply_err(req, EINVAL);
    return;
  }
  
  dir_inode = get_inode(parent);
  dir_size = inode_get_size(dir_inode);
  newdir_inode = get_inode(newparent);
  newdir_size = inode_get_size(newdir_inode);
  
  d_buf = malloc(dir_size);
  nd_buf = malloc(newdir_size + MAX_NAME_LEN + 4);
  
  if(d_buf == NULL || nd_buf == NULL) {
    free(d_buf);
    free(nd_buf);
    fuse_reply_err(req, ENOMEM);
    return;
  }

  inode_data_wrlock(dir_inode);
  
  read_inode_data(dir_inode, d_buf, 0, dir_size);
  dentry = sfs_find_dentry(d_buf, dir_size, name, strlen(name));
  assert(dentry);
  
  t_dentry = *dentry;
  
  sfs_remove_dentry(dir_inode, d_buf, dir_size, dentry);
  
  if(t_dentry.type == DENTRY_TYPE_DIR && parent != newparent)
    inode_dec_nlink(dir_inode);
  
  inode_data_unlock(dir_inode);
  
  
  inode_data_wrlock(newdir_inode);
  
  newdir_size = inode_get_size(newdir_inode);
  read_inode_data(newdir_inode, nd_buf, 0, newdir_size);
  dentry = sfs_find_dentry(nd_buf, newdir_size, newname, strlen(newname));
  
  if(dentry) {
    sfs_remove_dentry(dir_inode, nd_buf, newdir_size, dentry);
  }
  
  sfs_add_dentry(newdir_inode, t_dentry.ino, t_dentry.type, strlen(newname), newname);
  
  inode_data_unlock(newdir_inode);
  
  fuse_reply_err(req, 0);
}

static void sfs_symlink(fuse_req_t req, const char* link, 
           fuse_ino_t dir_ino, const char *name)
{
  struct sfs_inode* dir_inode;
  struct sfs_inode* inode;
  const struct fuse_ctx* ctx;
  struct fuse_entry_param e;
  uint64_t name_len;
  uint64_t link_size;
  
  dmsg("sfs_symlink() ino = %u name = %s", (uint32_t)dir_ino, name);
  
  name_len = strlen(name);
  if(name_len > MAX_NAME_LEN) {
    fuse_reply_err(req, ENAMETOOLONG);
    return;
  }
  
  link_size = strlen(link);
  
  ctx = fuse_req_ctx(req);
  
  inode = new_inode();
  inode->mode = S_IFLNK;
  inode->nlink = 1;
  inode->uid = ctx->uid;
  inode->gid = ctx->gid;
  inode->size = link_size;
  
  write_inode_data(inode, (void*)link, 0, link_size, true);
    
  dir_inode = get_inode(dir_ino);
  
  inode_data_wrlock(dir_inode);
  sfs_add_dentry(dir_inode, inode->ino, DENTRY_TYPE_SLINK, name_len, name);
  inode_data_unlock(dir_inode);
  
  
  fill_fuse_entry_param(inode, &e);
  fuse_reply_entry(req, &e);
}

static void sfs_readlink(fuse_req_t req, fuse_ino_t ino)
{
  struct sfs_inode* inode;
  char* link;
  uint64_t link_size;
  
  inode = get_inode(ino);
  
  link_size = inode_get_size(inode);
  
  link = malloc(link_size+1);
  if(link == NULL) {
    fuse_reply_err(req, ENOMEM);
    return;
  }
  
  inode_data_rdlock(inode);
  read_inode_data(inode, link, 0, link_size);
  inode_data_unlock(inode);
  
  link[link_size] = 0;
  fuse_reply_readlink(req, link);
  free(link);
}

static void sfs_open(fuse_req_t req, fuse_ino_t ino, struct fuse_file_info *fi)
{
  struct sfs_inode* inode;
  
  dmsg("sfs_open() ino = %u", (uint32_t)ino);
  
  inode = get_inode(ino);
  
  if(fi->flags & O_TRUNC) {
    inode_data_wrlock(inode);
    inode_truncate(inode, 0);
    inode_data_unlock(inode);
  }
  
  fi->fh = (uint64_t)inode;
  
	fuse_reply_open(req, fi);
}

static void sfs_read(fuse_req_t req, fuse_ino_t ino, size_t size,
			  off_t off, struct fuse_file_info *fi)
{
  struct sfs_inode* inode;
	char* buf;
  
  dmsg("sfs_read() ino = %u; off == %lu; size = %lu", (uint32_t)ino, off, size);
  
  inode = (struct sfs_inode*)fi->fh;

  inode_data_rdlock(inode);
  
  inode_wrlock(inode);
  
  if(off >= inode->size) {
    inode_unlock(inode);
    inode_data_unlock(inode);
    fuse_reply_buf(req, NULL, 0);
    return;
  }
  
  size = min(size, inode->size-off);
  
  inode_unlock(inode);
  
  buf = malloc(size);
  if(buf == NULL) {
    inode_data_unlock(inode);
    fuse_reply_err(req, ENOMEM);
    return;
  }
  
  inode_set_atime_now(inode);
  
  read_inode_data(inode, buf, off, size);
  inode_data_unlock(inode);
  
	fuse_reply_buf(req, buf, size);
  
  free(buf);
}

static void sfs_write(fuse_req_t req, fuse_ino_t ino, const char *buf, 
           size_t size, off_t off, struct fuse_file_info *fi)
{
  struct sfs_inode* inode;
  uint64_t ret;
  
  dmsg("sfs_write() ino = %u; off == %lu; size = %lu", (uint32_t)ino, off, size);
  
  if(off >= bptr3_lim || size >= bptr3_lim-off) {
    fuse_reply_err(req, EFBIG);
    return;
  }
  
  inode = (struct sfs_inode*)fi->fh;
  
  inode_set_mtime_now(inode);
  
  inode_data_rdlock(inode);
  
  ret = write_inode_data(inode, (void*)buf, off, size, false);
  
  inode_wrlock(inode);
  if(inode->size < off + ret)
    inode->size = off + ret;
  inode_unlock(inode);
  
  inode_data_unlock(inode);
  
  if(ret == 0) {
    fuse_reply_err(req, EDQUOT);
  } else {
    fuse_reply_write(req, ret);
  }
}

void sfs_statfs(fuse_req_t req, fuse_ino_t ino)
{
  struct statvfs stbuf;
  
  dmsg("sfs_statfs ino %u", (uint32_t)ino);
  
  memset(&stbuf, 0, sizeof(stbuf));
  stbuf.f_bsize = sb.block_size;
  stbuf.f_frsize = sb.block_size;
  stbuf.f_blocks = sb.total_blocks;
  stbuf.f_files = sb.total_inodes;
  stbuf.f_namemax = MAX_NAME_LEN;
  
  pthread_mutex_lock(&blocks_io_lock);
  stbuf.f_bfree = sb.free_blocks;
  stbuf.f_bavail = sb.free_blocks;
  pthread_mutex_unlock(&blocks_io_lock);
 
  pthread_mutex_lock(&inodes_io_lock);
  stbuf.f_ffree = sb.free_inodes;
  stbuf.f_favail = sb.free_inodes;
  pthread_mutex_unlock(&inodes_io_lock);
 
  fuse_reply_statfs(req, &stbuf);
}

void sfs_destroy(void* userdata)
{
  struct sfs_inode* inode;
    
  for(int idx = 0; idx < MAX_INODE_LISTS; ++idx) {
    pthread_rwlock_rdlock(&inode_list_lock[idx]);
    
    inode = inode_list[idx];
    while(inode) {
      inode_rdlock(inode);
      write_inode(inode);
      inode_unlock(inode);
      inode = inode->next;
    }
    
    pthread_rwlock_unlock(&inode_list_lock[idx]);
  }
  
  pthread_mutex_lock(&inodes_io_lock);
  pthread_mutex_lock(&blocks_io_lock);
  
  full_pwrite(gfd, bgi, sb.max_bgroup * sizeof(struct sfs_bgroup_info), sizeof(struct sfs_superblock));
  full_pwrite(gfd, &sb, sizeof(struct sfs_superblock), 0);

  pthread_mutex_unlock(&inodes_io_lock);
  pthread_mutex_unlock(&blocks_io_lock);
}

static struct fuse_lowlevel_ops sfs_ll_ops = {
  .forget = sfs_forget, 
  .getattr = sfs_getattr,
  .setattr = sfs_setattr,
  .lookup = sfs_lookup,
  .opendir = sfs_opendir,
  .releasedir = sfs_releasedir,
  .readdir = sfs_readdir,
  .create = sfs_create,
  .link = sfs_link,
  .unlink = sfs_unlink,
  .mkdir = sfs_mkdir,
  .rmdir = sfs_rmdir,
  .rename = sfs_rename,
  .symlink = sfs_symlink,
  .readlink = sfs_readlink,
  .open = sfs_open,
  .read = sfs_read,
  .write = sfs_write,
  .statfs = sfs_statfs,
  .destroy = sfs_destroy
};

int main(int argc, char* argv[])
{
  struct fuse_args args = FUSE_ARGS_INIT(argc-2, argv);
  struct fuse_session* se;
  struct stat stbuf;
  char* fsname;
  char* volume;
  char* mountpoint;
  
  if(argc < 3) {
    fprintf(stderr, "Usage:\t mount.sfs [-o option[,...]] <device|image_file> <mount_point>");
    exit(EXIT_FAILURE);
  }
  
  volume = argv[argc-2];
  mountpoint = argv[argc-1];
    
  gfd = open(volume, O_RDWR);
  if(gfd == -1) {
    perror("open");
    exit(EXIT_FAILURE);
  }
  
  full_pread(gfd, &sb, sizeof(sb), 0);
  
  if(sb.magic != MAGIC_VALUE) {
    fprintf(stderr, "%s isn't formatted as a sfs filesystem", argv[argc-2]);
    exit(EXIT_FAILURE);
  }
  
  fuse_opt_parse(&args, NULL, NULL, NULL);
  
  fuse_opt_add_arg(&args, "-odefault_permissions");
  
  fstat(gfd, &stbuf);
  if(stbuf.st_mode & S_IFBLK) {
    fuse_opt_add_arg(&args, "-oblkdev");
  
    fsname = malloc(strlen("-ofsname=") + strlen(volume) + 1);
    strcpy(fsname, "-ofsname=");
    strcat(fsname, volume);
    fuse_opt_add_arg(&args, fsname);
    free(fsname);
  }
  
  init_blocks_io();
  
  mountpoint = realpath(mountpoint, NULL);
  if(mountpoint == NULL) {
    perror("sfs: realpath()");
    exit(EXIT_FAILURE);
  }
  
  se = fuse_session_new(&args, &sfs_ll_ops, sizeof(sfs_ll_ops), NULL);
  if(se == NULL) {
    exit(EXIT_FAILURE);
  }

	if(fuse_set_signal_handlers(se) != 0)
    exit(EXIT_FAILURE);

	if(fuse_session_mount(se, mountpoint) != 0)
    exit(EXIT_FAILURE);
  
	if(fuse_daemonize(0) != 0) {
    fuse_session_unmount(se);
    exit(EXIT_FAILURE);
  }

	fuse_session_loop_mt(se, 1);
	
  fuse_session_unmount(se);
  fuse_session_destroy(se);

  return 0;
}
